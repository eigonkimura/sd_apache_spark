# Algoritmo baseado no site: http://gtezini.blogspot.com/2017/07/apache-spark-streaming-tutorial.html

from pyspark import SparkConf, SparkContext
from pyspark.streaming import StreamingContext
from pyspark.sql import Row, SQLContext
import sys
from textblob import TextBlob
import requests


def aggregate_tags_count(new_values, total_sum):
    return sum(new_values) + (total_sum or 0)


def get_sql_context_instance(spark_context):
    if ('sqlContextSingletonInstance' not in globals()):
        globals()['sqlContextSingletonInstance'] = SQLContext(spark_context)
    return globals()['sqlContextSingletonInstance']


# Cria a configuração do Spark
conf = SparkConf()

# Configura o nome do aplicativo Spark
conf.setAppName("TwitterStreamApp")

# Cria o contexto do Spark com as configurações padrões.
sc = SparkContext(conf=conf)

sc.setLogLevel("ERROR")

# Cria o contexto de streaming com base no contexto criado anteriormente. O inteiro
# indica o tempo de intervalo em segundos.
ssc = StreamingContext(sc, 2)

# Configura o checkpoint para permitir o RDD se recuperar.
ssc.checkpoint("checkpoint_TwitterApp")

# Cria o streaming de dados que recebe do localhost e da porta 9009.
dataStream = ssc.socketTextStream("localhost", 9009)


############################################################################
############################################################################
############################################################################
def process_rdd(time, rdd):
    print("----------- %s -----------" % str(time))
    try:
        # Recebe o SQL do Spark do cotnexto atual.
        sql_context = get_sql_context_instance(rdd.context)
        # Converte o RDD para Row RDD.
        row_rdd = rdd.map(lambda w: Row(sentimento=w[0], contagem=w[1]))
        # Cria o dataframe pela Row RDD.
        sentimentos_df = sql_context.createDataFrame(row_rdd)
        # Registra o dataframe na tabela.
        sentimentos_df.registerTempTable("sentimentos")
        # Recebe os três sentimentos da tabela do SQL, utilizando a requisição SQL.
        contagem_df = sql_context.sql(
            "select sentimento, contagem from sentimentos order by contagem desc limit 3")

        # Exibe no terminal o resultado.
        contagem_df.show()

        # Chama o método para exibir numa página web.
        # send_df_to_dashboard(contagem_df)
    except:
        e = sys.exc_info()[0]
        print("Error: %s" % e)


def send_df_to_dashboard(df):
    # Extrai os sentimentos do dataframe e converte para vetor.
    top_tags = [str(t.hashtag) for t in df.select("hashtag").collect()]
    # Extrai a quantidade do dataframe e converte para vetor.
    tags_count = [p.hashtag_count for p in df.select("hashtag_count").collect()]
    # Inicializa e envia os dados através do REST API.
    url = 'http://localhost:5001/updateData'
    # Estrutura os dados em dicionário.
    request_data = {'label': str(top_tags), 'data': str(tags_count)}
    # Envia a requisição post com o endereço e os dados.
    response = requests.post(url, data=request_data)


def analizar_sentimentos(line):
    # Instancia um objeto TextBlob
    texto = TextBlob(line)
    sentimento = None

    try:
        # Se o texto do tweet não estiver em inglês, é traduzido.
        # if texto.detect_language() != 'en':
        #     traducao = TextBlob(str(texto.translate(to='en')))
        #     sentimento = traducao.sentiment
        #
        # else:
        #     sentimento = texto.sentiment

        sentimento=texto.sentiment
        polaridade = sentimento.polarity
        resultado = None
        if polaridade > 0:
            resultado = "positivo"

        elif polaridade == 0:
            resultado = "neutro"

        else:
            resultado = "negativo"

    except:
        e = sys.exc_info()[0]
        print("Error: %s" % e)
        resultado = "err"
    return [resultado]


# Analisa os sentimentos de cada texto do tweet.
words = dataStream.flatMap(lambda line: analizar_sentimentos(line))

# Filtra as palavras que possui a letra 'o' e depois cria uma tupla com o sentimento e o valor 1.
sentimentos = words.filter(lambda w: 'o' in w).map(lambda x: (x, 1))

# Adiciona o contador de cada sentimento ao último resultado da contagem.
tags_totals = sentimentos.updateStateByKey(aggregate_tags_count)

# Processa cada RDD gerado em cada intervalo.
tags_totals.foreachRDD(process_rdd)

# Inicia a computação streaming.
ssc.start()

# Espera o streaming terminar.
ssc.awaitTermination()

