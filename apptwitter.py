# Algoritmo baseado no site: https://medium.com/@viniljf/criando-um-analisador-de-sentimentos-para-tweets-a53bae0c5147

from tweepy import StreamListener, OAuthHandler, Stream
import socket
import json

class MyListener(StreamListener):
    def on_data(self, dados):
        tweet = json.loads(dados)
        if "created_at" not in tweet:
            return

        send_tweets_to_spark(tweet, conn)
        return True


def send_tweets_to_spark(tweet, tcp_connection):
    try:
        tweet_text = tweet['text'] + '\n'
        print("Tweet Text: " + tweet_text)
        print("------------------------------------------")
        tcp_connection.send(tweet_text.encode())
    except Exception as e:
        print("ERROR =>", str(e))


if __name__ == "__main__":
    # Variáveis que recebem as autenticações.
    CONSUMER_KEY = '1d9trGy6NlTfQK4CAadsEywNa'
    CONSUMER_SECRET = 'tZt97WPCcI1A3LLUmcuZMeH7CYpCLUaqsRUjmdYG6fYAV6LTOo'

    ACCESS_TOKEN = '1003636786155966466-MLcOmtmxshRsYrp53K7m7eekVzMT92'
    ACCESS_TOKEN_SECRET = 'Vwv3EuMWs1IDy1gtFUv3bzguKJkWH0e7LJppwsIoUozJx'

    auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

    mylistener = MyListener()

    keywords = ['copa', 'copa2018', 'Copa', 'Copa2018']

    mystream = Stream(auth, listener=mylistener)

    TCP_IP = "localhost"
    TCP_PORT = 9009
    conn = None
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.listen(1)
    print("Waiting for TCP connection...")
    conn, addr = s.accept()
    print("Connected... Starting getting tweets.")

    mystream.filter(track=keywords, async=True)
